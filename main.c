#include <stdio.h>
#include <string.h>

int main(void) 
{
    /*Prompts containt standard end user instructions.*/
    const char PROMPT1[32] = {"Please enter your first name: \n"};
    const char PROMPT2[31] = {"Please enter your last name: \n"};

    /*firstName and lastName variables have been seperated out from original single variable.
    Single variable approach had garabage in the output after parsing in to seperate strings.*/
    char firstName[25];
    char lastName[25];

    /*size of each string respectively. Used to output size of strings in justified format.*/
    int firstNamesize = 0;
    int lastNamesize = 0;

    printf("%s", PROMPT1);
    scanf("%s", &firstName);
    firstNamesize = strlen(firstName);

    printf("%s", PROMPT2);
    scanf("%s", &lastName);
    lastNamesize = strlen(lastName);
    printf("\n");

    printf("%s %s\n", firstName, lastName);
    printf("%*d %*d\n",firstNamesize, firstNamesize, lastNamesize, lastNamesize);
    
    return 0;
}
